
#include <ps2joy.h>
#include <SPI.h>

#define ssPin 0

PS2Joystick gamepad = PS2Joystick(ssPin);

int hot2hat(bool u, bool d, bool l, bool r){
  if(u){
    if(l){
      return 315;
    }
    if(r){
      return 45;
    }
    return 0;
  }
  if(d){
    if(l){
      return 225;
    }
    if(r){
      return 135;
    }
    return 180;
  }
  if(l){
    return 270;
  }
  if(r){
    return 90;
  }
  return -1;
}

void setup() {
  pinMode(ssPin, OUTPUT);
  Serial.begin(9600);
  Joystick.useManualSend(true);
  SPI.begin();
}

void loop() {
  gamepad.update_();
  Joystick.X(gamepad.getLx()<<2);
  Joystick.Y(gamepad.getLy()<<2);
  Joystick.Z(gamepad.getRx()<<2);
  Joystick.Zrotate(gamepad.getRy()<<2);
  Joystick.sliderLeft(gamepad.getLz()<<2);
  Joystick.sliderRight(gamepad.getRz()<<2);
  Joystick.button(1, gamepad.getX());
  Joystick.button(2, gamepad.getO());
  Joystick.button(3, gamepad.getT());
  Joystick.button(4, gamepad.getS());
  Joystick.button(5, gamepad.getL1());
  Joystick.button(6, gamepad.getR1());
  Joystick.button(7, gamepad.getL2());
  Joystick.button(8, gamepad.getR2());
  Joystick.button(9, gamepad.getSelect());
  Joystick.button(10, gamepad.getStart());
  
  Joystick.button(11, gamepad.getL3());
  Joystick.button(12, gamepad.getR3());
  Joystick.button(13, gamepad.getU());
  Joystick.button(14, gamepad.getD());
  Joystick.button(15, gamepad.getL());
  Joystick.button(16, gamepad.getR());
  Joystick.hat(hot2hat(
    gamepad.getU(),gamepad.getD(),
    gamepad.getL(),gamepad.getR()
  ));
  Joystick.send_now();
  delay(20);
}
