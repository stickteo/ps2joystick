# ps2joystick

Another Teensy / Arduino library to interface a PS2 controller.

## features
* easy to use, just initialize SPI and initialize PS2 controller object
* hot plug-able
* simple api
* multiple controller support
* vibration support
* get pressure readings

## info

My main resource for developing the library comes from these websites:
* <http://store.curiousinventor.com/guides/PS2>
* <https://gist.github.com/scanlime/5042071>

They instruct on how to wire the circuit between the PS2 controller and the MCU. The minimum wires you need to wire are so:

PS2 Controller | MCU | Teensy 2.0
--- | --- | ---
Red | 5 V or 3.3 V | VCC
Black | Ground | GND
Brown | MISO | PIN 3
Orange | MOSI | PIN 2
Yellow | SS | PIN 0 (any IO pin)
Blue | CLK | PIN 1
Green | ACK | not implemented yet

> The ACK can be optional, but I found that compatibility with the MULTITAP may be related. Otherwise, the library seems to work with the PS2 controllers that I own.

Other optional wires:
PS2 Controller | Function
--- | ---
Grey | Motor Power
White | Unknown

## api

```cpp
PS2Joystick()
```
Assumes SS pin is pin 0.

```cpp
PS2Joystick(uint32_t ssPin)
```
The constructor. Just pass in the slave select pin for the respective controller. Useful when using multiple controllers. The library assumes only one SPI bus exists. Thus when wiring multiple controllers together, they should share the same pins except for the slave select pins (hence the name "slave select").

```cpp
void update_();
```
Polls the PS2 controller to send new button data and sends new motor values to the PS2 controller.

```cpp
void setRumble(uint8_t small, uint8_t large)
```
Set values for the rumble motors, the small motor will only run when passing in the max value 255. The large motor is variable.

```cpp
bool getX()
bool getO()
bool getT()
bool getS()
bool getL1()
bool getR1()
bool getL2()
bool getR2()
bool getSelect()
bool getStart()
bool getL3()
bool getR3()
bool getU()
bool getD()
bool getL()
bool getR()
uint8_t getLx()
uint8_t getLy()
uint8_t getLz()
uint8_t getRx()
uint8_t getRy()
uint8_t getRz()
```
Get button data. TODO: implement get raw data.

```cpp
PS2JStat getStatus_()
```
Status of controller, either NOT_CONFIGURED or READY. TODO: implement more configurations instead of having one default config.

## known issues

### multitap
Does not work with the MULTITAP. My guess is that I might need to support the ACK wire instead of using a blind delay. Not sure how to detect if MULTITAP exists or not, but not important.

An important issue is how the controllers can be accessed through the MULTITAP. Not sure if I missed something but I can't find any online docs on interfacing the MULTITAP.

However, I have some guesses on how it works:
* the first byte of the header likely indicates which controller to use (0x01 indicates controller A, but also the default value for a single controller)
* white wire is irrelevant, I have read somewhere it is for a light gun peripherals
* surprisingly, cheap ebay usb adapters support the multitap (though only controller A works) (this is why I think the white wire is irrelevant)

Thus in theory, maybe sending 0x02, 0x03, or 0x04 may get the MULTITAP to return the values for other controllers B, C, and D. Interestingly, this implies the MULTITAP actually converts the first byte of the header to 0x01. My initial guess was that the white wire was somehow related and weirdly multiplexes with the SS wire (like a 2nd slave select). However, the cheap ebay usb adapters proved me wrong since the white wire pin on its port is not even soldered!

### pressure values
Not an issue of the library but a consequence of trying to getting a pressure values of the controller at once. By default when the PS2 controller is first connected, it only sends digital readings. This fits into 2 bytes, and along with the 3 header byte, we get 5 byte transactions. Thus at 250 kHz baud, this equates to around 5 bytes * 8 bits / 250 kHz = 0.16 ms. Now consider having 19 byte transactions, this would equate to 0.608 ms! This severely eats into your CPU time! There are 2 ways to reduce the time, increase the baud, or select which button pressure to get (implement some sort of "profile" system).

## future features
* implement proper ACK instead of blind delay
* support the MULTITAP
* implement a sort of config profile
* get miscellaneous info from controller
* support a baud rate of 500kHz (may not be possible)

