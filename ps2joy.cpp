
#include "ps2joy.h"

#define SPI_SETTINGS (SPISettings(250000, LSBFIRST, SPI_MODE3))

// **************************************************

void sendFrame(uint8_t tx, uint8_t *rx);
void sendFrames(uint8_t *tx, uint8_t *rx, int n);
void sendFramesConstant(uint8_t tx, uint8_t *rx, int n);

#if DEBUG
void sprintHex(char *d, uint8_t *s, int n);
#endif

// **************************************************

void sendFrame(uint8_t tx, uint8_t *rx){
  *rx = SPI.transfer(tx);
  delayMicroseconds(12);
}

void sendFrames(uint8_t *tx, uint8_t *rx, int n){
  int i;
  for(i=0; i<n; i++){
    rx[i] = SPI.transfer(tx[i]);
    delayMicroseconds(12);
  }
}

void sendFramesConstant(uint8_t tx, uint8_t *rx, int n){
  int i;
  for(i=0; i<n; i++){
    rx[i] = SPI.transfer(tx);
    delayMicroseconds(12);
  }
}

#if DEBUG
void sprintHex(char *d, uint8_t *s, int n){
  int i,j;

  j=0;
  for(i=0; i<n; i++){
    sprintf(&d[j], "%02x ", s[i]);
    j += 3;
  }

  d[j] = '\n';
  d[j+1] = 0;
}
#endif

// **************************************************

void PS2Joystick::configure(){ 
  uint8_t rx[21];
  uint8_t header[3] = {0x01, 0x42, 0x00};

  #if DEBUG
  char buf[100];  
  #endif
  
  // initial poll
  rx[2] = 0;
  digitalWrite(ss,LOW);
  SPI.beginTransaction(SPI_SETTINGS);
  sendFrames(header, &rx[0], 3);
  sendFramesConstant(0xff, &rx[3], 2);
  SPI.endTransaction();
  digitalWrite(ss,HIGH);

  #if DEBUG
  Serial.print("01 42 00 ff ff\n");
  sprintHex(buf, rx, 5);
  Serial.print(buf);
  Serial.print("\n");
  #endif

  if(rx[2]!=0x5a){
    status_ = PS2JStat::NOT_CONFIGURED;
    return;
  }
  
  // enter configure mode
  header[1] = 0x43;
  digitalWrite(ss,LOW);
  SPI.beginTransaction(SPI_SETTINGS);
  sendFrames(header, &rx[0], 3);
  sendFrame(0x01, &rx[3]);
  sendFrame(0x00, &rx[4]);
  SPI.endTransaction();
  digitalWrite(ss,HIGH);

  #if DEBUG
  Serial.print("01 43 00 01 00\n");
  sprintHex(buf, rx, 5);
  Serial.print(buf);
  Serial.print("\n");
  #endif

  if(rx[2]!=0x5a){
    status_ = PS2JStat::NOT_CONFIGURED;
    return;
  }

  // turn on analog mode and lock
  header[1] = 0x44;
  digitalWrite(ss,LOW);
  SPI.beginTransaction(SPI_SETTINGS);
  sendFrames(header, &rx[0], 3);
  sendFrame(0x01, &rx[3]);
  sendFrame(0x03, &rx[4]);
  sendFramesConstant(0x00, &rx[5], 4);
  SPI.endTransaction();
  digitalWrite(ss,HIGH);

  #if DEBUG
  Serial.print("01 44 00 01 03 00 00 00 00\n");
  sprintHex(buf, rx, 9);
  Serial.print(buf);
  Serial.print("\n");
  #endif

  if(rx[2]!=0x5a){
    status_ = PS2JStat::NOT_CONFIGURED;
    return;
  }

  // map motors
  header[1] = 0x4d;
  digitalWrite(ss,LOW);
  SPI.beginTransaction(SPI_SETTINGS);
  sendFrames(header, &rx[0], 3);
  sendFrame(0x00, &rx[3]);
  sendFrame(0x01, &rx[4]);
  sendFramesConstant(0xff, &rx[5], 4);
  SPI.endTransaction();
  digitalWrite(ss,HIGH);

  #if DEBUG
  Serial.print("01 4d 00 00 01 ff ff ff ff\n");
  sprintHex(buf, rx, 9);
  Serial.print(buf);
  Serial.print("\n");
  #endif

  if(rx[2]!=0x5a){
    status_ = PS2JStat::NOT_CONFIGURED;
    return;
  }

  // turn on all pressure values
  header[1] = 0x4f;
  digitalWrite(ss,LOW);
  SPI.beginTransaction(SPI_SETTINGS);
  sendFrames(header, &rx[0], 3);
  sendFrame(0xff, &rx[3]);
  sendFrame(0xff, &rx[4]);
  sendFrame(0x03, &rx[5]);
  sendFramesConstant(0x00, &rx[6], 3);
  SPI.endTransaction();
  digitalWrite(ss,HIGH);

  #if DEBUG
  Serial.print("01 4f 00 ff ff 03 00 00 00\n");
  sprintHex(buf, rx, 9);
  Serial.print(buf);
  Serial.print("\n");
  #endif

  if(rx[2]!=0x5a){
    status_ = PS2JStat::NOT_CONFIGURED;
    return;
  }

  // exit config
  header[1] = 0x43;
  digitalWrite(ss,LOW);
  SPI.beginTransaction(SPI_SETTINGS);
  sendFrames(header, &rx[0], 3);
  sendFrame(0x00, &rx[3]);
  sendFramesConstant(0xff, &rx[4], 5);
  SPI.endTransaction();
  digitalWrite(ss,HIGH);

  #if DEBUG
  Serial.print("01 43 00 00 ff ff ff ff ff\n");
  sprintHex(buf, rx, 9);
  Serial.print(buf);
  Serial.print("\n");
  #endif

  if(rx[2]!=0x5a){
    status_ = PS2JStat::NOT_CONFIGURED;
    return;
  }

  status_ = PS2JStat::READY;
  return;
}

void PS2Joystick::update_(){
  if(status_ != PS2JStat::READY){
    configure();
    return;
  }

  int i;
  uint8_t rx[21];
  uint8_t header[3] = {0x01, 0x42, 0x00};

  #if DEBUG
  char buf[100];  
  #endif

  digitalWrite(ss,LOW);
  SPI.beginTransaction(SPI_SETTINGS);
  sendFrames(header, &rx[0], 3);
  sendFrame(motorS, &rx[3]);
  sendFrame(motorL, &rx[4]);
  sendFramesConstant(0x00, &rx[5], 16);
  SPI.endTransaction();
  digitalWrite(ss,HIGH);

  #if DEBUG
  Serial.print("01 42 00 ss ll 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00\n");
  sprintHex(buf, rx, 21);
  Serial.print(buf);
  Serial.print("\n");
  #endif

  if(rx[2]!=0x5a){
    status_ = PS2JStat::NOT_CONFIGURED;
    return;
  }

  digital[0] = ~rx[3];
  digital[1] = ~rx[4];
  for(i=0; i<16; i++){
    analog[i] = rx[5+i];  
  }

}

