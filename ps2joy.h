#ifndef PS2JOY_H
#define PS2JOY_H

#define DEBUG 1

#include <SPI.h>

enum class PS2JStat {NOT_CONFIGURED, READY};

class PS2Joystick {
  public:
    PS2Joystick(uint32_t ssPin){
      ss = ssPin;
      status_ = PS2JStat::NOT_CONFIGURED;
      motorS = 0; motorL = 0;
    }
    PS2Joystick(){
      ss = 0;
      status_ = PS2JStat::NOT_CONFIGURED;
      motorS = 0; motorL = 0;
    }
    void update_();
    
    bool getX()      { return (digital[1] & 0x40) >> 6; }
    bool getO()      { return (digital[1] & 0x20) >> 5; }
    bool getT()      { return (digital[1] & 0x10) >> 4; }
    bool getS()      { return (digital[1] & 0x80) >> 7; }
    bool getL1()     { return (digital[1] & 0x04) >> 2; }
    bool getR1()     { return (digital[1] & 0x08) >> 3; }
    bool getL2()     { return (digital[1] & 0x01); }
    bool getR2()     { return (digital[1] & 0x02) >> 1; }
    bool getSelect() { return (digital[0] & 0x01); }
    bool getStart()  { return (digital[0] & 0x08) >> 3; }
    bool getL3()     { return (digital[0] & 0x02) >> 1; }
    bool getR3()     { return (digital[0] & 0x04) >> 2; }
    bool getU()      { return (digital[0] & 0x10) >> 4; }
    bool getD()      { return (digital[0] & 0x40) >> 6; }
    bool getL()      { return (digital[0] & 0x80) >> 7; }
    bool getR()      { return (digital[0] & 0x20) >> 5; }
    uint8_t getLx() { return analog[2]; }
    uint8_t getLy() { return analog[3]; }
    uint8_t getLz() { return analog[14]; }
    uint8_t getRx() { return analog[0]; }
    uint8_t getRy() { return analog[1]; }
    uint8_t getRz() { return analog[15]; }

    void setRumble(uint8_t small, uint8_t large){
      motorS = small;
      motorL = large;
    }

    void sprintRaw(char *s);

    PS2JStat getStatus_(){
      return status_;
    }

  private:
    void configure();
    PS2JStat status_;
    uint32_t ss;
    uint32_t motorS, motorL;
    uint8_t digital[2];
    uint8_t analog[16];
};

#endif

